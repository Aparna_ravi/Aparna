var app = angular.module("myApp", []);
app.controller("myCtrl", function($scope) {
    $scope.heading = "Careers";
    $scope.contents = "We Aim at creating a better way to work in the <br> age of Digital Disruption.";
});