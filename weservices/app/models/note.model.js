const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    Name: String,
    Position: String,
    Office:String,
    Age: Number,
    Startdate: Date,
    Salary : Number


}
 );

module.exports = mongoose.model('Note', NoteSchema);
