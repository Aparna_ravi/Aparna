PROJECT TITLE
A Simple Web Service to Display Employee details


GETTING STARTED
->Installation of MongoDB,Nodejs,npm frameworks like express,mongoose etc.,


DEFINING API TO PERFORM CRUD OPERATIONS
->Define API to getall,getbyId,deleteall,deletebyid,post from the database
->Post the required number of records in DB

DISPLAYING RECORDS IN FRONTEND
->Call the get api which is defined in an ajax function
->The returned data will be in json format
->Display all the data
->Implement pagination,search,sorting for the records 



